#!/bin/bash

set -e

function check() {
  ruby app.rb "$2" | grep "$3" >/dev/null && echo "$1:ok - $3" || echo "$1:error - $3"        
}   

echo 'Running acceptance tests for gchecker'
check '01' 'spec/samples/Gemfile.valid' 'Gemfile is correct'
check '02' 'spec/samples/Gemfile.invalid.empty' 'Error: Gemfile is empty'
check '03' 'spec/samples/Gemfile.invalid.source' 'Error: Gemfile without source'
check '04' 'spec/samples/Gemfile.invalid.ruby' 'Error: Gemfile without ruby'
check '05' 'spec/samples/Gemfile.invalid.disordered' 'Error: Gemfile has disordered gems'
