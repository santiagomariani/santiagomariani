class Validator

  private

  def gemfile_contains_source(gemfile_content)
    gemfile_content.each_line do |line|
      return true if line.include? 'source'
    end
    return false
  end

  def gemfile_contains_ruby(gemfile_content)
    gemfile_content.each_line do |line|
      return true if /ruby '[0-9].[0-9].[0-9]'/.match?(line)
    end
    return false
  end

  def gemfile_content_is_empty(gemfile_content)
    return gemfile_content.empty?
  end

  def gemfile_gems_are_disordered(gemfile_content)
    prev_line = ''
    gemfile_content.each_line do |line|
      next unless line.start_with? 'gem'
      return true if prev_line > line
      prev_line = line
    end
    return false
  end

  public

  def process(gemfile_content)
    return 'Error: Gemfile is empty' if gemfile_content_is_empty gemfile_content
    return 'Error: Gemfile without source' unless gemfile_contains_source gemfile_content
    return 'Error: Gemfile without ruby' unless gemfile_contains_ruby gemfile_content
    return 'Error: Gemfile has disordered gems' if gemfile_gems_are_disordered gemfile_content
    return 'Gemfile is correct'
  end
end
