require 'rspec'
require_relative '../model/validator'

describe Validator do

  subject { @validator = Validator.new }

  it { should respond_to (:process) }

  it 'should return "Error: Gemfile is empty" when Gemfile is empty' do
    gemfile_string = ''
    result = subject.process gemfile_string
    expect(result).to eq 'Error: Gemfile is empty'
  end

  it 'should return "Gemfile is correct" when valid Gemfile' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.valid"
    validation_result = Validator.new.process gemfile_content
    expect(validation_result).to eq 'Gemfile is correct'
  end

  it 'should return "Error: Gemfile without source" when Gemfile has no source' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.invalid.source"
    validation_result = Validator.new.process gemfile_content
    expect(validation_result).to eq 'Error: Gemfile without source'
  end

  it 'should return "Error: Gemfile without ruby" when Gemfile has no ruby version' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.invalid.ruby"
    validation_result = Validator.new.process gemfile_content
    expect(validation_result).to eq 'Error: Gemfile without ruby'
  end

  it 'should return "Error: Gemfile has disordered gems" when Gemfile gems are disordered (alphabetically)' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.invalid.disordered"
    validation_result = Validator.new.process gemfile_content
    expect(validation_result).to eq 'Error: Gemfile has disordered gems'
  end


end
